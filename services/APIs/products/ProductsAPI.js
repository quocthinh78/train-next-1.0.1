import { doRequest } from "../../../utils/CoreUtils";

import { domains } from "../../../constants";

export default Object.freeze({
  getProducts: async ({ page, size } = {}) => {
    let url = `${domains.network}get?page=${page}&size=${size}`;

    return doRequest("get", url);
  },
});
