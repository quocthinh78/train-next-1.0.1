export function replaceProduct(arrReplace) {
  const arr = arrReplace.payload.items.map((product) => {
    const newKey = takeKeyToProvider(product.provider);
    const image = changImageToProvider(newKey);
    return {
      ...product,
      image,
    };
  });
  return {
    ...arrReplace,
    items: arr,
  };
}

export function changImageToProvider(key) {
  switch (key) {
    case "vinaphone":
      return "vinaphone";
    case "cmc":
      return "cmc";
    case "mobifone":
      return "mobifone";
    case "viettel":
      return "viettel";
    case "gvoice":
    case "gvoice1":
      return "gvoice";
    case "fpt":
      return "fpt";
    case "itel":
      return "itel";
    case "cgv":
      return "cgv";
    case "leon":
      return "vinaphone";
    default:
      return "";
  }
}
export function takeKeyToProvider(key) {
  const newKeyImage = key.split("_")[0];
  return newKeyImage;
}
