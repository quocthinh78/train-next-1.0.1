import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";

import {
  reduxCommonActions,
  reduxMappingActions,
  reduxMappingReducer,
} from "../../utils/CoreUtils";

/* ------------- Initial State ------------- */
const a = 1;
const INITIAL_STATE = Immutable({
  error: {},
  fetching: { products: true },
  content: {},
});

// https://quocthinh78:yh18FY2Yfxz-ofJaSpPc@gitlab.com/vihat-saas/crm/cloud.vihat.saas.input.ai.web.git
// https://gitlab.com/quocthinh78/train-next-1.0.1.git
/* ------------- Actions ------------- */

const actions = {
  ...reduxCommonActions,

  // classify: Products
  getProductsRequest: [
    ["classify", "params"],
    (state, { classify }) => {
      return state.merge({
        fetching: { ...state.fetching, [classify]: true },
        error: { ...state.error, [classify]: null },
      });
    },
  ],
  getProductsSuccess: [
    ["classify", "payload"],
    (state, { classify, payload }) => {
      return state.merge({
        fetching: { ...state.fetching, [classify]: false },
        content: { ...state.content, [classify]: payload },
      });
    },
  ],
};

/* ------------- Create Actions And Hookup Reducers To Types ------------- */

const { Types, Creators } = createActions(reduxMappingActions(actions));
const reducer = createReducer(
  INITIAL_STATE,
  reduxMappingReducer(actions, Types)
);

export { Types, reducer };
export default Creators;
