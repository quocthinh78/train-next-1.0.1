import itel from "./itel.png";
import fpt from "./fpt.png";
import mobifone from "./mobifone.png";
import cgv_mobifone from "./mobifone.png";
import cgv_viettel from "./mobifone.png";
import vinaphone from "./vinaphone.png";
import leon_vinaphone from "./vinaphone.png";
import cmc from "./cmc.png";
import vinaphone_cmc from "./cmc.png";
import cgv from "./cgv.png";
import viettel from "./viettel.png";
import gvoice from "./gvoice.png";
import gvoice1 from "./gvoice.png";

export default {
  gvoice1,
  vinaphone_cmc,
  leon_vinaphone,
  cgv_viettel,
  cgv_mobifone,
  itel,
  fpt,
  mobifone,
  vinaphone,
  cmc,
  cgv,
  viettel,
  gvoice,
};
