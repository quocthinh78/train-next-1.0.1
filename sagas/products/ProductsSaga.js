import { call, put, delay } from "redux-saga/effects";

import ProductsActions from "../../redux/products/ProductsRedux";

import ProductApis from "../../services/APIs/products/ProductsAPI";

import { getTimestamp, getDelayTime } from "../../utils/DateUtils";
import { validateResp, getErrorMsg } from "../../utils/StringUtils";

import * as CommonUtils from "../../utils/CommonUtils";
export function* getProducts(action) {
  const { classify, params } = action;
  try {
    let resp = yield call(ProductApis.getProducts, params);
    if (validateResp(resp)) {
      const respChangImage = CommonUtils.replaceProduct(resp);
      yield put(ProductsActions.getProductsSuccess(classify, respChangImage));
    } else throw resp;
  } catch (error) {
    yield put(
      ProductsActions.productsCommonFailure(classify, getErrorMsg(error))
    );
  }
}
