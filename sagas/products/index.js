import { sagasMappingEffects } from "../../utils/CoreUtils";

import { Types } from "../../redux/products/ProductsRedux";
import * as Sagas from "./ProductsSaga";

const keys = [["getProducts"]];

/* ------------- Connect Types To Sagas ------------- */

export default sagasMappingEffects(keys, Types, Sagas);
