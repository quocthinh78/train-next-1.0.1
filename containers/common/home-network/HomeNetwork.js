import React, { Suspense, lazy } from "react";
import { compose } from "redux";
import { connect } from "react-redux";

// hocs
import withAuth from "../../../hocs/AuthHocs";

// actions
import ProductsActions from "../../../redux/products/ProductsRedux";

// service

// utils

// common

// styles
import { withStyles } from "@material-ui/core/styles";
import { styles } from "./styles";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import LoopIcon from "@mui/icons-material/Loop";

// componetns
import Header from "./Header";
import PaginationButton from "./PaginationButton";
const ProductItem = lazy(() => import("./ProductItem"));
// assets

// variables
class HomeNetwork extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      classify: {
        products: "products",
      },
      pagination: {
        page: 1,
        size: 15,
      },
      filterPage: {
        total_pages: 2,
      },
    };
  }

  componentDidMount() {
    const { classify, pagination } = this.state;

    this.props.getProducts(classify.products, pagination);
  }

  // shouldComponentUpdate(prevProps, nextProps) {
  //   console.log("pr", prevProps);
  //   console.log("n", nextProps);
  //   return true;
  // }
  // componentDidUpdate() {
  //   console.log("aa");
  //   const { classify, pagination } = this.state;
  //   this.props.getProducts(classify.products, pagination);

  //   // if (this.state.a === "thinh") {
  //   //   const { classify } = this.state;

  //   //   const { productsContents, productsFetching } = this.props;
  //   //   if (!productsFetching[classify.products]) {
  //   //     this.setState((state) => {
  //   //       return {
  //   //         ...state,
  //   //         filterPage: {
  //   //           page_number: productsContents.products.payload.page_number,
  //   //           total_pages: productsContents.products.payload.total_pages,
  //   //         },
  //   //       };
  //   //     });
  //   //   }
  //   //   this.state.a = "toan";
  //   // }
  // }

  _loading = () => {
    return (
      <Box
        sx={{
          width: "100%",
          dispay: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "400px",
        }}
      >
        <LoopIcon />
        <LoopIcon />
        <LoopIcon />
        <LoopIcon />
        <LoopIcon />
        <LoopIcon />
      </Box>
    );
  };

  changPage = (page) => {
    const { classify } = this.state;
    const { productsContents } = this.props;

    this.props.getProducts(classify.products, {
      page,
      size: 15,
    });
    this.setState((state) => {
      return {
        ...state,
        pagination: {
          page,
          size: 15,
        },
      };
    });
  };

  render() {
    const { classes, productsContents } = this.props;
    return (
      <div className={classes.wrapper}>
        <div className={classes.container}>
          <Container sx={{ minWidth: "1200px" }}>
            <Header />
            <Suspense fallback={this._loading()}>
              <Box>
                <ProductItem productsContents={productsContents} />
              </Box>
              <PaginationButton
                productsContents={productsContents}
                pagination={this.state.pagination}
                changPage={this.changPage}
              />
            </Suspense>
          </Container>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { accessToken } = state.session;
  return {
    // session
    accessToken,
    // products
    productsFetching: state.products.fetching,
    productsContents: state.products.content,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getProducts: (classify, params) =>
    dispatch(ProductsActions.getProductsRequest(classify, params)),
});

export default compose(
  withAuth(),
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(HomeNetwork);
