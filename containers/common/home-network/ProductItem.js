import React, { Fragment } from "react";
import { compose } from "redux";

// hocs
import withAuth from "../../../hocs/AuthHocs";

// actions

// service

// utils

// common

// styles
import { withStyles } from "@material-ui/core/styles";
import { styles } from "./styles";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import { CardActionArea } from "@mui/material";
import Switch from "@mui/material/Switch";

// componetns

// assets
import ProductImage from "../../../assets/images/common";
// variables
const label = { inputProps: { "aria-label": "Switch demo" } };
class ProductItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes, productsContents } = this.props;
    return (
      <Grid container spacing={3}>
        {productsContents.products &&
          productsContents.products.payload.items.map((item) => {
            const { _id, number, provider } = item;
            return (
              <Fragment key={_id}>
                <Grid item lg={4} sm={6}>
                  <Card sx={{ borderRadius: 2 }}>
                    <CardActionArea sx={{ p: 2 }}>
                      <div className={classes.top}>
                        <img
                          className={classes.imgTop}
                          src={ProductImage[provider]}
                          alt="img"
                        />
                        <div className={classes.statusTop}>
                          <div className={classes.cycleActive}></div>
                          <div className={classes.dateActive}>
                            Dang hoat dong (14/12/2000)
                          </div>
                        </div>
                      </div>
                      <div className={classes.center}>
                        <div className={classes.left}>
                          <div className={classes.phone}>{number}</div>
                          <div className={classes.itemStatus}>
                            <img
                              className={classes.itemImg}
                              src="https://img.icons8.com/ios/50/000000/incoming-call.png"
                              alt="image"
                            />
                            <div className={classes.itemText}>
                              Thinh bo lo 1 cuoc goi
                            </div>
                          </div>
                          <div className={classes.itemStatus}>
                            <img
                              className={classes.itemImg}
                              src="https://img.icons8.com/ios/50/000000/outgoing-call.png"
                              alt="image"
                            />
                            <div className={classes.itemText}>
                              Thinh bo lo 1 cuoc goi
                            </div>
                          </div>
                          <div className={classes.itemStatus}>
                            <img
                              className={classes.itemImg}
                              src="https://img.icons8.com/ios/50/000000/speech-bubble-with-dots.png"
                              alt="image"
                            />
                            <div className={classes.itemText}>
                              Thinh bo lo 1 cuoc goi
                            </div>
                          </div>
                          <div className={classes.itemStatus}>
                            <img
                              className={classes.itemImg}
                              src="https://img.icons8.com/dotty/80/000000/call-list.png"
                              alt="image"
                            />
                            <div className={classes.itemText}>
                              Thinh bo lo 1 cuoc goi
                            </div>
                          </div>
                        </div>
                        <div className={classes.right}>
                          <Switch {...label} defaultChecked />
                        </div>
                      </div>
                    </CardActionArea>
                  </Card>
                </Grid>
              </Fragment>
            );
          })}
      </Grid>
    );
  }
}

export default compose(withAuth(), withStyles(styles))(ProductItem);
