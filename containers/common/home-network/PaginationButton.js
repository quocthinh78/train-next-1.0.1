import React from "react";
import { compose } from "redux";

// hocs
import withAuth from "../../../hocs/AuthHocs";

// actions

// service

// utils

// common

// styles
import { withStyles } from "@material-ui/core/styles";
import { styles } from "./styles";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";

// componetns

// assets

// variables
class PaginationButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  _createButton = (total_pages, pagination) => {
    const { classes, changPage } = this.props;
    let html = [];
    for (let i = 0; i < total_pages; i++) {
      if (i + 1 === pagination.page) {
        html.push(
          <div
            key={i}
            onClick={() => changPage(i + 1)}
            className={classes.paginationItemActive}
          >
            {i + 1}
          </div>
        );
      } else {
        html.push(
          <div
            key={i}
            onClick={() => changPage(i + 1)}
            className={classes.paginationItem}
          >
            {i + 1}
          </div>
        );
      }
    }
    return html;
  };

  render() {
    const { productsContents, pagination, classes, changPage } = this.props;
    const total_pages = productsContents.products?.payload.total_pages;
    return (
      <Box sx={{ marginTop: "15px" }}>
        <div className={classes.pagination}>
          <div className={classes.paginationLeft}>
            <div className={classes.mr}>15 đầu số mỗi trang</div>
            <img
              className={classes.arrow}
              src="https://img.icons8.com/material-sharp/24/000000/give-way--v1.png"
            />
            <div className={classes.ml}>1 - 15 trong 18 danh sách đầu số</div>
          </div>
          <div className={classes.paginationRight}>
            <Button
              disabled={pagination.page <= 1}
              className={classes.paginationButton}
              onClick={() => changPage(pagination.page - 1)}
            >
              <img src="https://img.icons8.com/ios-glyphs/30/000000/chevron-left.png" />
            </Button>
            {this._createButton(total_pages, pagination)}
            <Button
              className={classes.paginationButton}
              onClick={() => changPage(pagination.page + 1)}
              disabled={pagination.page >= total_pages}
            >
              <img src="https://img.icons8.com/ios-glyphs/30/000000/chevron-right.png" />
            </Button>
          </div>
        </div>
      </Box>
    );
  }
}

export default compose(withAuth(), withStyles(styles))(PaginationButton);
