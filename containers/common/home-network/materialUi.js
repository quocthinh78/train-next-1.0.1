import { styled } from "@mui/material/styles";
import Button from "@mui/material/Button";
import { purple } from "@mui/material/colors";
import { textColors, colors, boxShadows } from "../../../assets/styles/Theme";
const BlueButton = styled(Button)(({ theme }) => ({
  color: theme.palette.getContrastText(purple[500]),
  backgroundColor: colors.blue,
  boxShadow: "0 4px 16px 0 rgba(87, 191, 219, 0.5)",
  textTransform: "capitalize",
  "&:hover": {
    backgroundColor: textColors.blueHover,
  },
}));

const WhiteButton = styled(Button)(({ theme }) => ({
  backgroundColor: colors.white,
  color: colors.black,
  textTransform: "capitalize",
  "&:hover": {
    backgroundColor: boxShadows.grayLightHover,
  },
}));
const PaginationButton = styled(Button)(({ theme }) => ({
  backgroundColor: colors.white,
  color: colors.black,
  borderRadius: 0,
  padding: "5px",
  width: 10,
  marginLeft: 12,
  textTransform: "capitalize",
  "&:hover": {
    backgroundColor: boxShadows.grayLightHover,
  },
}));

export { BlueButton, WhiteButton, PaginationButton };
