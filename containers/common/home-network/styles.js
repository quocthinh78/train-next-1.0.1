import { getShadowStyle } from "../../../utils/StyleUtils";

import { cStyles } from "../../../assets/styles";
import {
  borderRadiuses,
  fontFamilys,
  fontSizes,
  textColors,
  colors,
  boxShadows,
  baseHeights,
} from "../../../assets/styles/Theme";

export const styles = (theme) => ({
  wrapper: {
    width: "100%",
    height: "100%",
    overflow: "auto",
    display: "flex",
    justifyContent: "center",
  },
  imgBel: {
    width: 20,
    height: 20,
    marginLeft: 30,
  },
  imgUser: {
    width: 30,
    height: 30,
    borderRadius: "50%",
    marginRight: 6,
  },
  userBox: {
    display: "flex",
    alignItems: "center",
  },
  wrapNameUser: {
    display: "flex",
    alignItems: "center",
  },
  arrow: {
    width: 10,
    height: 10,
    marginLeft: 4,
  },
  flag: {
    width: 24,
    height: 24,
    marginLeft: 30,
  },
  userNav: {
    display: "flex",
    justifyContent: "center",
    marginLeft: 30,
    alignItems: "center",
  },
  top: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "start",
    marginBottom: "10px",
  },
  imgTop: {
    width: 70,
    height: 30,
  },
  statusTop: {
    display: "flex",
    alignItems: "center",
  },
  cycleActive: {
    background: boxShadows.greenHover,
    borderRadius: "50%",
    width: 10,
    height: 10,
    marginRight: 5,
  },
  dateActive: {
    color: boxShadows.greenHover,
  },
  center: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "flex-end",
  },
  phone: {
    fontSize: fontSizes.regular,
    fontWeight: 500,
    marginBottom: "10px",
  },
  itemStatus: {
    display: "flex",
    alignItems: "center",
    padding: "5px 0",
  },
  itemText: {
    color: textColors.placeholder,
  },
  itemImg: {
    width: 20,
    height: 20,
    marginRight: "5px",
  },
  pagination: {
    display: "flex",
    paddingBottom: 80,
    justifyContent: "space-between",
    alignItems: "center",
  },
  paginationLeft: {
    display: "flex",
    alignItems: "center",
  },
  mr: {
    marginRight: 20,
  },
  ml: {
    marginLeft: 20,
  },
  paginationRight: {
    display: "flex",
    alignItems: "center",
  },
  paginationItem: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: 30,
    height: 30,
    margin: "0 4px",
    background: "#fff",
    "& .active": {
      color: "red",
    },
  },
  paginationItemActive: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: 30,
    height: 30,
    margin: "0 4px",
    background: colors.orange,
  },
  paginationButton: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: 30,
    height: 30,
    marginLeft: 8,
    fontSize: "14px !important",
    outlineStyle: "none",
    "& img": {
      width: 10,
      height: 10,
    },
  },
});
