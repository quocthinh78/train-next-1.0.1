import React, { Fragment } from "react";
import { compose } from "redux";

// hocs
import withAuth from "../../../hocs/AuthHocs";

// actions

// service

// utils

// common

// styles
import { withStyles } from "@material-ui/core/styles";
import { styles } from "./styles";
import Box from "@mui/material/Box";
import CardMedia from "@mui/material/CardMedia";
import InputLabel from "@mui/material/InputLabel";
import { BlueButton, WhiteButton } from "./materialUi";


// componetns

// assets

// variables

const label = { inputProps: { "aria-label": "Switch demo" } };
class Header extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { classes } = this.props;
    return (
      <Fragment>
        <Box
          sx={{
            display: "flex",
            justifyContent: "flex-start",
            alignItems: "center",
            marginBottom: "10px",
            padding: "10px 0",
          }}
        >
          <Box sx={{ mr: 3 }}>Cấu hình / Tổng đài / Đầu số</Box>
          <Box
            sx={{
              borderRadius: "4px",
              background: "#fff",
              boxShadow: "0 4px 16px 0 rgb(73,73,73,0.5)",
              flexGrow: 1,
            }}
          >
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <Box sx={{ padding: "10px" }}>
                <CardMedia
                  component="img"
                  height="20"
                  image="https://img.icons8.com/ios/100/000000/search--v1.png"
                  alt="green iguana"
                />
              </Box>
              <Box>
                <InputLabel sx={{ width: "200px", padding: "10px" }} />
              </Box>
            </Box>
          </Box>
          <Box>
            <div className={classes.userBox}>
              <img
                className={classes.imgBel}
                src="https://img.icons8.com/ios/50/000000/bell.png"
                alt="bel"
              />
              <div className={classes.userNav}>
                <img
                  className={classes.imgUser}
                  src="https://scontent.fdad3-1.fna.fbcdn.net/v/t1.6435-1/cp0/p50x50/48374641_510949816071785_2432688181036974080_n.jpg?_nc_cat=106&ccb=1-5&_nc_sid=7206a8&_nc_ohc=pd-pmnhHzREAX8euDbr&_nc_ht=scontent.fdad3-1.fna&oh=ddaf1691f09cf42eea7a1ae5dc2d89d6&oe=616C34D3"
                />
                <div className={classes.wrapNameUser}>
                  <p>Quoc Thinh</p>
                  <img
                    className={classes.arrow}
                    src="https://img.icons8.com/material-sharp/24/000000/give-way--v1.png"
                  />
                </div>
              </div>
            </div>
          </Box>
          <Box>
            <img
              className={classes.flag}
              src="https://img.icons8.com/color/48/000000/vietnam-circular.png"
            />
          </Box>
        </Box>
        <Box sx={{ display: "flex" }}>
          <Box sx={{ marginBottom: "25px" }}>
            <BlueButton sx={{ mr: 2 }} variant="contained">
              Danh sách đầu số
            </BlueButton>
            <WhiteButton sx={{ mr: 2 }} variant="contained">
              Kịch bản cuộc gọi
            </WhiteButton>
            <WhiteButton sx={{ mr: 2 }} variant="contained">
              Nhóm nội bộ
            </WhiteButton>
            <WhiteButton sx={{ mr: 2 }} variant="contained">
              Nhóm bên ngoài
            </WhiteButton>
            <WhiteButton sx={{ mr: 2 }} variant="contained">
              Tương tác trong phim
            </WhiteButton>
            <WhiteButton sx={{ mr: 2 }} variant="contained">
              Bố cục
            </WhiteButton>
          </Box>
        </Box>
      </Fragment>
    );
  }
}

export default compose(withAuth(), withStyles(styles))(Header);
