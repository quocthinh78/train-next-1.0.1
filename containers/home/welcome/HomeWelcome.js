import React from "react";
import { compose } from "redux";
import { connect } from "react-redux";

// hocs
import withAuth from "../../../hocs/AuthHocs";

// actions
import SessionActions from "../../../redux/common/SessionRedux";
import UserActions from "../../../redux/user/UserRedux";
import RoleActions from "../../../redux/user/RoleRedux";

// utils
import { getTokenContent } from "../../../utils/WebUtils";

// styles
import { withStyles } from "@material-ui/core/styles";
import { styles } from "./styles";

import Box from "@mui/material/Box";
import Slider from "@mui/material/Slider";

import Switch from "@mui/material/Switch";

const label = { inputProps: { "aria-label": "Switch demo" } };

import Button from "@material-ui/core/Button";

const StyledButton = withStyles({
  root: {
    background: "linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)",
    borderRadius: 3,
    border: 0,
    color: "white",
    height: 48,
    padding: "0 30px",
    boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
  },
  label: {
    textTransform: "capitalize",
  },
})(Button);
const StyledSwitch = withStyles({
  root: {
    boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
  },
  label: {
    textTransform: "capitalize",
  },
  thumb: {
    color: "red",
  },
})(Switch);

class HomeWelcome extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      classify: {
        user: "userInfo",
        role: "roles",
      },
      switch: true,
    };
  }

  componentDidMount() {
    const { classify } = this.state;
    this.props.getUserInfo(classify.user, {
      id: getTokenContent("contact_id"),
    });
    this.props.getRoles(classify.role, { page: 1, size: 15 });
  }

  _renderUserInfo = () => {
    const { classify } = this.state;
    const { classes, userFetching, userContent } = this.props;
    return (
      <div className={classes.dataField}>
        {userFetching[classify.user]
          ? "Loading..."
          : JSON.stringify(userContent[classify.user])}
      </div>
    );
  };

  _renderRole = (roleContent) => {
    const { classify } = this.state;

    console.log("dsadsad", this.props.roleContent);
    return <div>{JSON.stringify(roleContent[classify.role])}</div>;
  };
  _renderListRole = () => {
    const { classify } = this.state;
    const { classes, roleFetching, roleContent } = this.props;
    return (
      <div className={classes.dataField}>
        {roleFetching[classify.role]
          ? "Loading..."
          : this._renderRole(roleContent)}
      </div>
    );
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.wrapper}>
        <div className={classes.container}>
          <p>Welcome to this testing page!</p>
          <button onClick={() => this.props.onLogout(true)}>Đăng xuất</button>
          {this._renderUserInfo()}
          {this._renderListRole()}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { accessToken } = state.session;
  return {
    // session
    accessToken,
    // user
    userFetching: state.user.fetching,
    userContent: state.user.content,
    // role
    roleFetching: state.role.fetching,
    roleContent: state.role.content,
  };
};

const mapDispatchToProps = (dispatch) => ({
  // session
  onLogout: (isRedirect) => dispatch(SessionActions.sessionLogout(isRedirect)),
  // user
  getUserInfo: (classify, params) =>
    dispatch(UserActions.getUserInfoRequest(classify, params)),
  // role
  getRoles: (classify, params) =>
    dispatch(RoleActions.getRolesRequest(classify, params)),
});

export default compose(
  withAuth(),
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(HomeWelcome);
